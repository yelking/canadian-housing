# Houssing Prices in Canada.
## Author
_Youssef ELouatouati_
## Programming Language
+ This project using R 4.1.0 and R studio 1.4.1717 as the programing language and versions for all the caculation. The function that used for model selection is "step(nullmodel,scope=list(upper=fullmodel),direction="both")" with stepwise selection. The function for generating MLR is "lm" with auto-generation.
## File Description 
### data.csv 
+ This file contains the data for this MLR prediction
### R_Script.r 
+ This file includes all the R code: the transformation of categorical data, selection of outliers, and fixing the missing data in some predictors ect...  
### model_selection.r 
+ Contains the source file for stepwise selection for the MLR   
### analysis.pdf 
+ Contains the analysis of the data, accuracy on the model selection, and the interpretation of the final MLR model. By using the following techniques such as by not limited to residual plot with predictors, the residual plot with the response, residual plot, and QQ plot ect...

